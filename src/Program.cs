﻿using System;
using Akka.Actor;
using Akka.DI.Core;
using Akka.DI.Ninject;
using AkkaConsole.Db.Redis;
using AkkaConsole.Db.Redis.Interfaces;
using AkkaConsole.Db.Repositories;
using AkkaConsole.Db.Repositories.Interfaces;
using AkkaConsole.Extensions;
using AkkaConsole.KataLimitNotificationEmail.Actors;
using AkkaConsole.KataLimitNotificationEmail.Messages;
using AkkaConsole.KataShoppingWeb;
using AkkaConsole.KataShoppingWeb.Messages;
using Ninject;

namespace AkkaConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Kata Hello World
            //var offerActor = actorSystem.ActorOf(Props.Create( () => new OfferActor()));

            //for( var i=0; i<5; i++)
            //    offerActor.Tell(new Offer("preventivo 1"));
            //Console.ReadLine();
            #endregion

            #region circuit braker
            //var dangerousCrudActor = actorSystem.ActorOf(Props.Create(() => new DangerousCrudActor()));

            //1000.CycleWithInterval(() =>
            //    {
            //        dangerousCrudActor.Tell(new ProductMessage());
            //    });
            #endregion

            #region Kata LimitNotificationEmail

            //IKernel container = new StandardKernel();
            //container.Bind<IRedisDatabaseFactory>().To(typeof(RedisDatabaseFactory)).InSingletonScope();
            //container.Bind<IProductRepository>().To<ProductRepository>();
            //container.Bind<IInvoiceRepository>().To<InvoiceRepository>();

            //var actorSystem = ActorSystem.Create("myactorsystem");
            //var resolver = new NinjectDependencyResolver(container, actorSystem);

            //var prop = actorSystem.DI().Props<CrudActor>();
            //var crudActor = actorSystem.ActorOf(prop);
            //1000.CycleWithInterval(() =>
            //{
            //    crudActor.Tell(new ProductMessage());
            //    //crudActor.Tell(new InvoiceMessage(10));
            //});

            #endregion

            #region Kata ShoppingWeb
            var actorSystem = ActorSystem.Create("myactorsystem");
            var actor = actorSystem.ActorOf(Props.Create(() => new ShoppingCartStateMachine()));
            actor.Tell(new CartItemAdded("my username"));
            actor.Tell(new OrderSubmitted(Guid.NewGuid(), Guid.NewGuid(), "my username"));
            Console.ReadLine();

            #endregion
        }
    }
}