﻿using System;

namespace AkkaConsole.KataShoppingWeb.Data
{
    public interface IShoppingCart
    {
        string CurrentState { get; set; }

        string UserName { get; set; }

        DateTime Created { get; set; }
        DateTime Updated { get; set; }

        Guid? ExpirationId { get; set; }

        Guid? OrderId { get; set; }

        Guid CorrelationId { get; set; }
    }
}