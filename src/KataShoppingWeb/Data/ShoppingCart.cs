﻿using System;

namespace AkkaConsole.KataShoppingWeb.Data
{
    public class ShoppingCart : IShoppingCart
    {
        public ShoppingCart()
        {
            CorrelationId = Guid.NewGuid();
        }

        public string CurrentState { get; set; }

        public string UserName { get; set; }

        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        
        public Guid? ExpirationId { get; set; }

        public Guid? OrderId { get; set; }

        public Guid CorrelationId { get; set; }
    }
}
