﻿using System;

namespace AkkaConsole.KataShoppingWeb.Messages
{
    public class CartItemAdded : IMessages
    {
        public DateTime Timestamp { get; private set;  }
        public string UserName { get; private set; }

        public CartItemAdded(string username)
        {
            if (string.IsNullOrEmpty(username))
                username = "Unknown";
            this.UserName = username;
            this.Timestamp = DateTime.UtcNow;
        }
    }
}
