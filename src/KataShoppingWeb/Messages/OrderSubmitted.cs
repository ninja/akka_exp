﻿using System;

namespace AkkaConsole.KataShoppingWeb.Messages
{
    public class OrderSubmitted : IMessages
    {
        public Guid OrderId { get; private set; }

        public DateTime Timestamp { get; private set; }

        public Guid CartId { get; private set; }

        public string UserName { get; private set; }

        public OrderSubmitted(Guid orderId, Guid cartId, string username)
        {
            this.UserName = username;
            this.OrderId = orderId;
            this.CartId = cartId;
            this.Timestamp = DateTime.UtcNow;
        }
    }
}
