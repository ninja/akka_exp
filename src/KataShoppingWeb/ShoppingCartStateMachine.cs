﻿using System;
using Akka.Actor;
using AkkaConsole.KataShoppingWeb.Data;
using AkkaConsole.KataShoppingWeb.Messages;

namespace AkkaConsole.KataShoppingWeb
{
    public class ShoppingCartStateMachine : FSM<States, IShoppingCart>
    {
        public ShoppingCartStateMachine()
        {
            When(States.ItemAdded, e=>
            {
                if (e.FsmEvent is CartItemAdded)
                {
                    var data = e.FsmEvent as CartItemAdded;
                    e.StateData.Created = data.Timestamp;
                    e.StateData.Updated = data.Timestamp;
                    e.StateData.UserName = data.UserName;
                    Console.Out.WriteLineAsync(
                        $"Item Added in ShoppingCart: {e.StateData.UserName} to {e.StateData.CorrelationId}");
                    return GoTo(States.Submitted).Using(e.StateData).ForMax(TimeSpan.FromSeconds(1));
                }
                return Stay();
            });

            When(States.Submitted, e =>
            {
                if (e.FsmEvent is OrderSubmitted)
                {
                    var data = e.FsmEvent as OrderSubmitted;
                    e.StateData.OrderId = data.OrderId;
                    Console.Out.WriteLineAsync(
                        $"Cart Submitted: {e.StateData.UserName} to {e.StateData.CorrelationId}");
                }
                return Stay().Using(e.StateData);
            });

            StartWith(States.ItemAdded, new ShoppingCart());
            Initialize();
        }
    }
}