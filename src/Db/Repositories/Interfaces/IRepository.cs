﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaConsole.Db.Repositories.Interfaces
{
    public interface IRepository<T> where T : class
    {
        object Insert(T message);
    }
}
