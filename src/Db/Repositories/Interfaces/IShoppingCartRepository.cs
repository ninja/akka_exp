﻿using AkkaConsole.KataShoppingWeb.Data;

namespace AkkaConsole.Db.Repositories.Interfaces
{
    public interface IShoppingCartRepository : IRepository<ShoppingCart>
    {
    }
}
