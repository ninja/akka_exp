﻿using AkkaConsole.KataLimitNotificationEmail.Messages;

namespace AkkaConsole.Db.Repositories.Interfaces
{
    public interface IInvoiceRepository : IRepository<InvoiceMessage>
    {
    }
}
