﻿using System;
using AkkaConsole.Db.Redis.Interfaces;
using AkkaConsole.Db.Repositories.Interfaces;
using AkkaConsole.KataShoppingWeb.Data;

namespace AkkaConsole.Db.Repositories
{
    public class ShoppingCartRepository : IShoppingCartRepository
    {
        private readonly IRedisDatabaseFactory _redisDatabaseFactory;

        public ShoppingCartRepository(IRedisDatabaseFactory redisDatabaseFactory)
        {
            _redisDatabaseFactory = redisDatabaseFactory;
        }

        public object Insert(ShoppingCart message)
        {
            throw new NotImplementedException();
        }
    }
}
