﻿using System;
using AkkaConsole.Db.Redis;
using AkkaConsole.Db.Redis.Interfaces;
using AkkaConsole.Db.Repositories.Interfaces;
using AkkaConsole.KataLimitNotificationEmail.Messages;
using Newtonsoft.Json;

namespace AkkaConsole.Db.Repositories
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private readonly IRedisDatabaseFactory _redisDatabaseFactory;

        public InvoiceRepository(IRedisDatabaseFactory redisDatabaseFactory)
        {
            _redisDatabaseFactory = redisDatabaseFactory;
        }

        public object Insert(InvoiceMessage message)
        {
            var key = message.Id.ToString().GetBaseKeyInvoice(message);
            try
            {
                var db = _redisDatabaseFactory.GetDatabase();
                db.StringSet(key, JsonConvert.SerializeObject(message), new TimeSpan(0, 0, 5, 0));
                return key;
            }
            catch (Exception exception)
            {
                return new EmailMessage(message);
            }
        }
    }
}
