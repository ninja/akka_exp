﻿using StackExchange.Redis;

namespace AkkaConsole.Db.Redis.Interfaces
{
    public interface IRedisDatabaseFactory
    {
        IDatabase GetDatabase();
    }
}
