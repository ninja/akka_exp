﻿using AkkaConsole.KataLimitNotificationEmail.Messages;

namespace AkkaConsole.Db.Redis
{
    public static class Key
    {
        public static string GetBaseKeyProduct(this string key, DataMessage message)
        {
            return $"key_product_{GetKey(key, message)}";
        }

        public static string GetBaseKeyInvoice(this string key, DataMessage message)
        {
            return $"key_invoice_{GetKey(key, message)}";
        }

        private static string GetKey(string key, DataMessage message)
        {
            return $"{key}_{message.Date.Hour}_{message.Date.Minute}_{message.Date.Second}";
        }
    }
}