﻿using System;
using System.Net;
using AkkaConsole.Db.Redis.Interfaces;
using StackExchange.Redis;

namespace AkkaConsole.Db.Redis
{
    public class RedisDatabaseFactory : IRedisDatabaseFactory
    {
        private readonly Lazy<IConnectionMultiplexer> _lazyConnectionMultiplexer;

        public RedisDatabaseFactory()
        {
            var endPoint = new DnsEndPoint("localhost", 6379);

            var configOptions = new ConfigurationOptions
            {
                EndPoints = { endPoint },
                ConnectTimeout = 5000,
                AbortOnConnectFail = false
            };

            _lazyConnectionMultiplexer = new Lazy<IConnectionMultiplexer>(() =>
                ConnectionMultiplexer.Connect(configOptions));
        }

        private IConnectionMultiplexer Connection => _lazyConnectionMultiplexer.Value;

        public IDatabase GetDatabase()
        {
            if (!Connection.IsConnected)
                throw new Exception("Redis connection failure");
            return Connection.GetDatabase();
        }
    }
}
