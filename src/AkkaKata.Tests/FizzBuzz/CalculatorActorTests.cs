﻿using Akka.Actor;
using AkkaKata.FizzBuzz;
using Xunit;

namespace AkkaKata.Tests.FizzBuzz
{
    public class CalculatorActorTests : Akka.TestKit.Xunit2.TestKit
    {
        [Theory]
        [InlineData(3, "Fizz")]
        [InlineData(5, "Buzz")]
        [InlineData(15, "FizzBuzz")]
        [InlineData(4, "4")]
        public void CalculatorNumber(int n, string expectedMessage)
        {
            var actor = Sys.ActorOf(Props.Create(() => new CalculatorActor()));
            actor.Tell(n);
            ExpectMsg(expectedMessage);
        }
    }
}
