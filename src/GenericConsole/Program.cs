﻿using System;
using System.IO;
using Akka.Actor;
using AkkaKata.MapReduceCsvMarvel.Actors;
using AkkaKata.MapReduceCsvMarvel.Messages;
using CsvHelper;
using CsvHelper.Configuration;

namespace GenericConsole
{
    class Program
    {
        private static string PathMarvelCsv => Utilities.GetPathTestProject() + @"\Files\marvel-wikia-data.csv";

        static void Main(string[] args)
        {
            StartMapReduceExperiment();
        }

        private static void StartMapReduceExperiment()
        {
            var actorSystem = ActorSystem.Create("actorsystem");
            var csvRowActor = actorSystem.ActorOf(Props.Create(() => new CsvRowActor()));

            using (var streamReader = new StreamReader(PathMarvelCsv))
            {
                var csv = new CsvReader(streamReader, new CsvConfiguration {IgnoreHeaderWhiteSpace = true});
                while (csv.Read())
                {
                    var comicCharacter = ComicCharacterMessage.Create(
                        csv.GetField<int>(0),
                        csv.GetField<string>(1),
                        csv.GetField<string>(4),
                        csv.GetField<string>(7),
                        !string.IsNullOrWhiteSpace(csv.GetField<string>(12)) ? csv.GetField<int>(12) : -1);
                    csvRowActor.Tell(comicCharacter);
                }
            }
            csvRowActor.Tell(PoisonPill.Instance);
            Console.ReadLine();
        }
    }
}
