﻿using System.IO;
using System.Reflection;

namespace GenericConsole
{
    internal static class Utilities
    {
        private static string GetPathProjectRunning(Assembly projectServices)
        {
            var directoryProjectTests = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            if (directoryProjectTests == null) return string.Empty;
            var nameProjectTests = directoryProjectTests.Name;
            var nameProjectServices = projectServices.GetName().Name;
            var pathProjectServices = directoryProjectTests.FullName.Replace(nameProjectTests, nameProjectServices);
            var directory = new DirectoryInfo(pathProjectServices);
            return directory.Exists ? pathProjectServices : string.Empty;
        }

        public static string GetPathTestProject()
        {
            var assemblyCurrent = typeof(Utilities).Assembly;
            var pathProject = GetPathProjectRunning(assemblyCurrent);
            return pathProject;
        }
    }
}
