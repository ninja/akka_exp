﻿using System;

namespace AkkaConsole.Extensions
{
    public static class Extensions
    {
        public static void CycleWithInterval( this int interval, Action action)
        {
            while (true)
            {
                System.Threading.Thread.Sleep(interval);
                action();
            }
        }
    }
}
