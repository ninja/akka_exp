﻿using System;
using Akka.Actor;
using Akka.Pattern;
using AkkaConsole.KataLimitNotificationEmail.Messages;

namespace AkkaConsole.InternalCircuitBreaker.Actors
{
    public class DangerousCrudActor : ReceiveActor
    {
        private readonly CircuitBreaker _breaker;
        private const int _minutes = 12;

        public DangerousCrudActor()
        {
            _breaker = new CircuitBreaker(5, TimeSpan.FromSeconds(20), TimeSpan.FromMinutes(1));


            Receive<ProductMessage>(m =>
            {
                var startKoProduct = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, _minutes, 10);
                var endKoProduct = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, _minutes, 20);
                var rangeDateKoDatabaseDuringInsertProduct = new RangeDate(startKoProduct, endKoProduct);

                try
                {
                    _breaker.WithCircuitBreaker(() =>
                    {
                        var result = FakeRepository.Insert(m, rangeDateKoDatabaseDuringInsertProduct);
                        if (result.Exception == null)
                            Console.WriteLine("ok");
                        return result;
                    });
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            });
        }
    }
}