﻿using System;
using Akka.Actor;
using AkkaConsole.KataHelloWorld.Messages;

namespace AkkaConsole.KataHelloWorld.Actors
{
    public class OfferActor : TypedActor, IHandle<Offer>
    {
        private int _index = 0;
        public void Handle(Offer message)
        {
            _index++;
            Console.WriteLine(message.Description);
            Console.WriteLine(_index);
            var selection = Context.ActorOf(Props.Create( () => new GenericActor()));
            selection.Tell(new Error("message error"));
        }
    }
}