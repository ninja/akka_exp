﻿using System;
using Akka.Actor;
using AkkaConsole.KataHelloWorld.Messages;

namespace AkkaConsole.KataHelloWorld.Actors
{
    public class GenericActor : UntypedActor
    {
        protected override void OnReceive(object message)
        {
            if (message is Error)
            {
                Console.WriteLine(((Error)message).Message);
                //Context.Sender.Tell(new Offer("sono tornato indietro"));
            }
        }
    }
}