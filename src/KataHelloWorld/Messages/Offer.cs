﻿using System;

namespace AkkaConsole.KataHelloWorld.Messages
{
    public class Offer
    {
        public Guid Id { get; private set; }
        public string Description { get; private set; }

        public Offer( string description)
        {
            this.Id = Guid.NewGuid();
            this.Description = description;
        }
    }
}