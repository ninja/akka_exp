﻿using System;
using System.Collections.Generic;
using Akka.Actor;
using AkkaConsole.Db.Repositories.Interfaces;
using AkkaConsole.KataLimitNotificationEmail.Messages;

namespace AkkaConsole.KataLimitNotificationEmail.Actors
{
    public class CrudActor : ReceiveActor, IWithUnboundedStash
    {
        public IStash Stash { get; set; }
        private readonly Dictionary<string, int> _values = new Dictionary<string, int>();

        public CrudActor(IProductRepository productRepository, IInvoiceRepository invoiceRepository)
        {
            Receive<ProductMessage>(m =>
            {
                var productMessage = productRepository.Insert(m);
                if (productMessage is EmailMessage)
                    this.Receive(productMessage);
                else
                {
                    Stash.UnstashAll();
                    if (_values.ContainsKey(typeof(ProductMessage).ToString()))
                        _values[typeof(ProductMessage).ToString()] = 0;
                    Console.WriteLine(productMessage);
                }
            });

            Receive<InvoiceMessage>(m =>
            {
                var invoiceMessage = invoiceRepository.Insert(m);
                if (invoiceMessage is EmailMessage)
                    this.Receive(invoiceMessage);
                else
                {
                    Stash.UnstashAll();
                    if (_values.ContainsKey(typeof(InvoiceMessage).ToString()))
                        _values[typeof(InvoiceMessage).ToString()] = 0;
                    Console.WriteLine(invoiceMessage);
                }
            });

            this.Receive<EmailMessage>(msg =>
            {
                Stash.Stash();
                if (_values.ContainsKey(msg.Type))
                {
                    _values[msg.Type] += 1;
                    if (_values[msg.Type] <= 10)
                        Console.WriteLine($"Invio email n°: {_values[msg.Type]} {msg.Body}");
                }
                else
                {
                    _values.Add(msg.Type, 1);
                    Console.WriteLine($"Invio email n°: {_values[msg.Type]} {msg.Body}");
                }
            });
        }
    }
}
