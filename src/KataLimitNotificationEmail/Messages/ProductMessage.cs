﻿namespace AkkaConsole.KataLimitNotificationEmail.Messages
{
    public class ProductMessage : DataMessage
    {
        public ProductMessage()
        {
            this.Description = $"id: {this.Id} date: {this.Date}";
        }

        public string Description { get; private set; }
    }
}