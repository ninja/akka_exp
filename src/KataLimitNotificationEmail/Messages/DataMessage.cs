﻿using System;

namespace AkkaConsole.KataLimitNotificationEmail.Messages
{
    public class DataMessage
    {
        public DataMessage()
        {
            this.Id = Guid.NewGuid();
            this.Date = DateTime.Now;
        }

        public Guid Id { get; private set; }
        public DateTime Date { get; private set; }
    }
}
