﻿namespace AkkaConsole.KataLimitNotificationEmail.Messages
{
    public class EmailMessage
    {
        public EmailMessage(DataMessage message)
        {
            this.Type = message.GetType().ToString();
            this.Body = $"body email: {message.Id} {Type}";
        }

        public string Body { get; private set; }
        public string Type { get; private set; }
    }
}
