﻿namespace AkkaConsole.KataLimitNotificationEmail.Messages
{
    public class InvoiceMessage : DataMessage
    {
        public InvoiceMessage(decimal value)
        {
            this.Value = value;
        }

        public decimal Value { get; private set; }
    }
}