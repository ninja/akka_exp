﻿using System;
using System.Threading.Tasks;

namespace AkkaConsole.KataLimitNotificationEmail.Messages
{
    public class RangeDate
    {
        public RangeDate (DateTime start, DateTime end)
        {
            this.Start = start;
            this.End = end;
        }

        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }
    }

    public class FakeRepository
    {
        public static Task Insert(ProductMessage message, RangeDate rangeDateKoDatabaseDuringInsertProduct)
        {
            var insertResult = BaseRepository.Insert(message, rangeDateKoDatabaseDuringInsertProduct);
            if (insertResult is EmailMessage)
            {
                Console.WriteLine("send email");
                return Task.FromException(new Exception("test exception"));
            }
            return Task.FromResult("ok");
        }
    }

    public class BaseRepository
    {
        public static object Insert(DataMessage message, RangeDate rangeDate)
        {
            if (DateTime.Now > rangeDate.Start && DateTime.Now <= rangeDate.End)
                return new EmailMessage(message);

            return "OK";
        }
    }
}