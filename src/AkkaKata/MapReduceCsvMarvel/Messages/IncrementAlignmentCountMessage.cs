﻿
namespace AkkaKata.MapReduceCsvMarvel.Messages
{
    public class IncrementAlignmentCountMessage
    {
        public string Align { get; }

        private IncrementAlignmentCountMessage(string align)
        {
            this.Align = align;
        }

        public static IncrementAlignmentCountMessage Create(string align)
        {
            return new IncrementAlignmentCountMessage(align);
        }
    }
}