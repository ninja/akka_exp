﻿
namespace AkkaKata.MapReduceCsvMarvel.Messages
{
    public class IncrementSexCharacterCountMessage
    {
        public string Sex { get; }

        private IncrementSexCharacterCountMessage(string sex)
        {
            this.Sex = sex;
        }

        public static IncrementSexCharacterCountMessage Create(string sex)
        {
            return new IncrementSexCharacterCountMessage(sex);
        }
    }
}
