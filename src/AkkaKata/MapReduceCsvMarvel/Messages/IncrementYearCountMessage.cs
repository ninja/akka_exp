﻿
namespace AkkaKata.MapReduceCsvMarvel.Messages
{
    public class IncrementYearCountMessage
    {
        public int Year { get; }

        public IncrementYearCountMessage(int year)
        {
            this.Year = year;
        }

        public static IncrementYearCountMessage Create(int year)
        {
            return new IncrementYearCountMessage(year);
        }
    }
}
