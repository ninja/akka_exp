﻿
namespace AkkaKata.MapReduceCsvMarvel.Messages
{
    public class ComicCharacterMessage
    {
        public int Id { get; }
        public string Name { get; }
        public string Align { get; }
        public string Sex { get; }
        public int Year { get; }

        private ComicCharacterMessage(int id, string name, string align, string sex, int year)
        {
            this.Id = id;
            this.Name = name;
            this.Align = align;
            this.Sex = sex;
            this.Year = year;
        }

        public static ComicCharacterMessage Create(int id, string name, string align, string sex, int year)
        {
            return new ComicCharacterMessage(id, name, align, sex, year);
        }
    }
}
