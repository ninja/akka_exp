﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using AkkaKata.MapReduceCsvMarvel.Messages;

namespace AkkaKata.MapReduceCsvMarvel.Actors
{
    public class AlignmentCharacterActor : ReceiveActor
    {
        private readonly Dictionary<string, int> _statistics;

        public AlignmentCharacterActor()
        {
            _statistics = new Dictionary<string, int>();
            Receive<IncrementAlignmentCountMessage>(message => HandleAlignmentMessage(message));
        }

        private void HandleAlignmentMessage(IncrementAlignmentCountMessage message)
        {
            if (_statistics.ContainsKey(message.Align))
                _statistics[message.Align] += 1;
            else
                _statistics.Add(message.Align, 1);
        }

        public override void AroundPostStop()
        {
            foreach (var key in _statistics.Keys.OrderBy(x => x))
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"{key}:{_statistics[key]}");
            }

            base.AroundPostStop();
        }
    }
}
