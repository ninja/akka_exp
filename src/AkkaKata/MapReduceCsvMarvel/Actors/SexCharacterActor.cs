﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using AkkaKata.MapReduceCsvMarvel.Messages;

namespace AkkaKata.MapReduceCsvMarvel.Actors
{
    public class SexCharacterActor : ReceiveActor
    {
        private readonly Dictionary<string, int> _statistics;

        public SexCharacterActor()
        {
            _statistics = new Dictionary<string, int>();
            Receive<IncrementSexCharacterCountMessage>(message => HandleSexCharacter(message));
        }

        private void HandleSexCharacter(IncrementSexCharacterCountMessage message)
        {
            if (_statistics.ContainsKey(message.Sex))
                _statistics[message.Sex] += 1;
            else
                _statistics.Add(message.Sex, 1);

            //if( _statistics[message.Sex] == 100)
            //    throw new SuspendException();
        }

        public override void AroundPostStop()
        {
            foreach (var key in _statistics.Keys.OrderBy( x => x))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{key}:{_statistics[key]}");
            }

            base.AroundPostStop();
        }
    }
}
