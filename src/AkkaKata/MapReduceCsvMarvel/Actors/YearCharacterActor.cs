﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using AkkaKata.MapReduceCsvMarvel.Messages;

namespace AkkaKata.MapReduceCsvMarvel.Actors
{
    public class YearCharacterActor : ReceiveActor
    {
        private readonly Dictionary<int, int> _statistics;

        public YearCharacterActor()
        {
            _statistics = new Dictionary<int, int>();
            Receive<IncrementYearCountMessage>(message => HandleYearCharacter(message));
        }

        private void HandleYearCharacter(IncrementYearCountMessage message)
        {
            if (_statistics.ContainsKey(message.Year))
                _statistics[message.Year] += 1;
            else
                _statistics.Add(message.Year, 1);
        }

        public override void AroundPostStop()
        {
            foreach (var key in _statistics.Keys.OrderBy(x => x))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"{key}:{_statistics[key]}");
            }

            base.AroundPostStop();
        }
    }
}
