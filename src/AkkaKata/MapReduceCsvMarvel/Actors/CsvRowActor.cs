﻿using Akka.Actor;
using AkkaKata.MapReduceCsvMarvel.Exceptions;
using AkkaKata.MapReduceCsvMarvel.Messages;

namespace AkkaKata.MapReduceCsvMarvel.Actors
{
    public class CsvRowActor : ReceiveActor
    {
        private readonly IActorRef _sexCharacterActor;
        private readonly IActorRef _yearCharacterActor;
        private readonly IActorRef _alignmentCharacterActor;

        private readonly IActorRef _statisticsActor;

        public CsvRowActor()
        {
            _sexCharacterActor = Context.ActorOf(Props.Create(() => new SexCharacterActor()));
            _yearCharacterActor = Context.ActorOf(Props.Create(() => new YearCharacterActor()));
            _alignmentCharacterActor = Context.ActorOf(Props.Create(() => new AlignmentCharacterActor()));

            _statisticsActor = Context.ActorOf(Props.Create(() => new StatisticsActor()));

            Receive<ComicCharacterMessage>(message => HandleComicCharacter(message));
        }

        private void HandleComicCharacter(ComicCharacterMessage message)
        {
            _sexCharacterActor.Tell(IncrementSexCharacterCountMessage.Create(message.Sex));
            _yearCharacterActor.Tell(IncrementYearCountMessage.Create(message.Year));
            _alignmentCharacterActor.Tell(IncrementAlignmentCountMessage.Create(message.Align));

            //_statisticsActor.Tell(IncrementSexCharacterCountMessage.Create(message.Sex));
            //_statisticsActor.Tell(IncrementYearCountMessage.Create(message.Year));
            //_statisticsActor.Tell(IncrementAlignmentCountMessage.Create(message.Align));
        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(1, 1000,
                x =>
                {
                    if (x is SuspendException)
                        return Directive.Resume;

                    return Directive.Restart;
                });
        }
    }
}
