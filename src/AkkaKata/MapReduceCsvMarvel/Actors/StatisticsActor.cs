﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using AkkaKata.MapReduceCsvMarvel.Messages;

namespace AkkaKata.MapReduceCsvMarvel.Actors
{
    public class StatisticsActor : ReceiveActor
    {
        private readonly Dictionary<string, int> _sexCharacter;
        private readonly Dictionary<int, int> _yearCharacter;
        private readonly Dictionary<string, int> _alignCharacter;

        public StatisticsActor()
        {
            _sexCharacter = new Dictionary<string, int>();
            _alignCharacter = new Dictionary<string, int>();
            _yearCharacter =new Dictionary<int, int>();

            Receive<IncrementAlignmentCountMessage>(message => HandleAlignmentCharacter(message));
            Receive<IncrementYearCountMessage>(message => HandleYearCharacter(message));
            Receive<IncrementSexCharacterCountMessage>(message => HandleSexCharacter(message));
        }

        private void HandleSexCharacter(IncrementSexCharacterCountMessage message)
        {
            if (_sexCharacter.ContainsKey(message.Sex))
                _sexCharacter[message.Sex] += 1;
            else
                _sexCharacter.Add(message.Sex, 1);
        }

        private void HandleYearCharacter(IncrementYearCountMessage message)
        {
            if (_yearCharacter.ContainsKey(message.Year))
                _yearCharacter[message.Year] += 1;
            else
                _yearCharacter.Add(message.Year, 1);
        }

        private void HandleAlignmentCharacter(IncrementAlignmentCountMessage message)
        {
            if (_alignCharacter.ContainsKey(message.Align))
                _alignCharacter[message.Align] += 1;
            else
                _alignCharacter.Add(message.Align, 1);
        }

        public override void AroundPostStop()
        {
            foreach (var key in _yearCharacter.Keys.OrderBy(x => x))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"{key}:{_yearCharacter[key]}");
            }

            foreach (var key in _alignCharacter.Keys.OrderBy(x => x))
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"{key}:{_alignCharacter[key]}");
            }

            foreach (var key in _sexCharacter.Keys.OrderBy(x => x))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{key}:{_sexCharacter[key]}");
            }

            base.AroundPostStop();
        }
    }
}