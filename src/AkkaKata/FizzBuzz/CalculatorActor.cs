﻿using Akka.Actor;

namespace AkkaKata.FizzBuzz
{
    public class CalculatorActor : ReceiveActor
    {
        public CalculatorActor()
        {
            Receive<int>(message => ReceiveNumberMessage(message));
        }

        private void ReceiveNumberMessage(int number)
        {
            Sender.Tell(GetMessage(number));
        }

        private static string GetMessage(int number)
        {
            if (number % 3 == 0 && number % 5 == 0)
                return "FizzBuzz";
            if (number % 3 == 0)
                return "Fizz";
            if (number % 5 == 0)
                return "Buzz";
            return number.ToString();
        }
    }
}